package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends Controller {

    public static void index() {
        render();
    }

    /**
     * Список словарей
     */
    public static void dictionaries() {

        render();
    }

    /**
     * Список слов в данном словаре
     */
    public static void dictionary(Long id) {

        render();
    }

    /**
     * Создание/редактирование слова
     */
    public static void word() {

        render();
    }

}