package controllers;

import models.User;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.Http;
import play.mvc.With;

import java.security.MessageDigest;

@With(Secure.class)
public class Security extends Secure.Security {
    private static final String KEY = "tqerr";
    private static final String KEY_IP = "localhost";

    static boolean authenticate(String username, String password) {
        Http.Header header = request.headers.get("realip");
        String key = header == null ? KEY_IP : header.value();

        if (Cache.get(key) != null && (Integer) Cache.get(key) > 2) {
            Logger.info("Not auth, block - [%s][remoteAddress:%s]", username, key);

            flash.put("loginerror", Messages.get("server.error.loginBlock"));
            return false;
        }

        User user = User.find("username = ? AND password = ?", username, getHash(password)).first();
        if (user != null) {
            session.put("i", Crypto.encryptAES(String.valueOf(user.id)));
//            session.put("g", Crypto.encryptAES(user.getGroup().toString()));

            Logger.info("Auth - [%s][remoteAddress:%s]", username, key);
            return true;
        } else {
            flash.put("loginerror", Messages.get("server.error.wrongLogin"));

            if (Cache.get(key) == null) {
                Cache.set(key, 1, "10min");
            } else {
                Cache.set(key, (Integer) Cache.get(key) + 1, "10min");
            }
            return false;
        }
    }

    static void onDisconnected() {
        redirect("/");
    }

    static boolean check(String value) {
        User user;
        try {
            String id = Crypto.decryptAES(session.get("i"));
            user = User.findById(Long.parseLong(id));

            if (user != null && user.getGroup() != null) {
                if (user.getGroup() == User.Group.valueOf(value)) {
                    return true;
                }
            }
        } catch (Throwable e) {
            return false;
        }

        return false;
    }

    public static String getHash(String value) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(value.getBytes("UTF-8"));
            byte[] mdbytes = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < mdbytes.length; i++) {
                hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
            }

            return hexString.toString();
        } catch (Exception e) {
            Logger.error(e.getMessage(), e);
            return value;
        }
    }

}