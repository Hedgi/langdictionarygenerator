package injection;

import com.google.inject.AbstractModule;
import org.aeonbits.owner.ConfigFactory;
import play.Play;
import properties.CommonProperty;

public class WorldinmeModule extends AbstractModule {

    @Override
    protected void configure() {

        bind(CommonProperty.class).toInstance(ConfigFactory.create(CommonProperty.class, Play.configuration));

//        bind(DataParser.class).toInstance(new DataParserImpl());
//        bind(BitcointWallet.class).toInstance(new BitcointWalletLocalImpl());
//
//        bind(PriceHelper.class).toInstance(new PriceHelper());

    }
}
