package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class User extends Model {

    @Required
    @Column(columnDefinition = "VARCHAR(30) default ''", nullable = false)
    private String username;
    @Required
    @Column(columnDefinition = "VARCHAR(100) default ''", nullable = false)
    private String password;

    @Column(columnDefinition = "VARCHAR(5) default 'USER'", name = "ggroup")
    @Enumerated(EnumType.STRING)
    private Group group;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public enum Group {
        USER, ADMIN
    }
}
