package models.lang;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class LangDictionary extends Model {

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CodeLang code;

    @Column(columnDefinition = "INT(3)")
    private Integer sortOrder = 0;

    @OneToMany(mappedBy = "dictionary")
    private List<WordHeap> heap;

    public LangDictionary() {
    }

    public LangDictionary(String name, CodeLang code) {
        this.name = name;
        this.code = code;
        this.sortOrder = 0;
    }

    public List<WordHeap> getHeap() {
        if (heap == null) {
            heap = new ArrayList<>();
        }
        return heap;
    }

    public void setHeap(List<WordHeap> heap) {
        this.heap = heap;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CodeLang getCode() {
        return code;
    }

    public void setCode(CodeLang code) {
        this.code = code;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
}
