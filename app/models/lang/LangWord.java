package models.lang;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class LangWord extends Model {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private WordHeap heap;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CodeLang code;

    @ElementCollection
    private List<String> sameWords;

    @Column(columnDefinition = "VARCHAR(255)", nullable = false)
    private String text;


    public WordHeap getHeap() {
        return heap;
    }

    public void setHeap(WordHeap heap) {
        this.heap = heap;
    }

    public CodeLang getCode() {
        return code;
    }

    public void setCode(CodeLang code) {
        this.code = code;
    }

    public List<String> getSameWords() {
        if (sameWords == null) {
            sameWords = new ArrayList<>();
        }
        return sameWords;
    }

    public void setSameWords(List<String> sameWords) {
        this.sameWords = sameWords;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
