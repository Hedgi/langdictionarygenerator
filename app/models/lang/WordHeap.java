package models.lang;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class WordHeap extends Model {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private LangDictionary dictionary;

    @OneToMany(mappedBy = "heap")
    private List<LangWord> words;

    public LangDictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(LangDictionary dictionary) {
        this.dictionary = dictionary;
    }

    public List<LangWord> getWords() {
        if (words == null) {
            words = new ArrayList<>();
        }
        return words;
    }

    public void setWords(List<LangWord> words) {
        this.words = words;
    }
}
