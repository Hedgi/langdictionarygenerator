package properties;

import org.aeonbits.owner.Config;

public interface CommonProperty extends Config {

    @Key("service.email")
    String getServiceEmail();

    @Key("service.email.admins")
    @Separator(",")
    String[] getAdminEmails();

    @Key("google.map.api.key")
    String getGoogleMapKey();
}
